
import { 
  FETCH_TODOS,
  FETCH_SUCCESS,
  FETCH_FAILED,
  POST_TODO,
  POST_TODO_SUCCESS,
  POST_TODO_FAILED,
} from '../constants';

//Saga effects
import { put, takeLatest, call, race } from 'redux-saga/effects';
import { buildURL, buildHaders } from './helperAction';
// import { resolve } from 'url';

const delay = (ms) => new Promise((reject) => {
  setTimeout(() => {
    reject(new Error('Request Error'));
  }, ms)
})

function* fetchTodos(params) {
  try {
    const { endpoint, method, data, headers } = params
    const url =  yield call(buildURL, endpoint)

    const requestHeaders = yield call(buildHaders, method, data, headers)

    const {response, timeout} = yield race({
      response: call(fetch, url, requestHeaders),
      timeout: call(delay, 10000)
    })

    if (response) {
      const payload = yield call(response.json.bind(response))
      yield put({ type: FETCH_SUCCESS, payload });   
    } else {
      yield put({ type: FETCH_FAILED, timeout });
    }
  } catch (error) {        
    yield put({ type: FETCH_FAILED, error });
  }
}


function* addNewTodo(todo) {
  const headers = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title: todo,
      userId: 1,
      completed: true
    }),
  };
  const url = 'https://jsonplaceholder.typicode.com/posts';
  try {
    const response = yield call(fetch, url, headers);
    
    if (response.status >= 200 && response.status < 300) {

      const payload = yield call(response.json.bind(response))
      
      yield put({ type: POST_TODO_SUCCESS, payload });
    
    } else {
      throw response;
    }

  } catch (e) {

    yield put({ type: POST_TODO_FAILED, e })
  }
}

export function* watchFetchTodo() {
  yield takeLatest(FETCH_TODOS, fetchTodos);
}

export function* watchAddNewTodo() {
  yield takeLatest(POST_TODO, addNewTodo)
}