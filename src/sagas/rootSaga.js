import { all, fork } from 'redux-saga/effects';

import { watchFetchTodo, watchAddNewTodo } from './todoSagas';

export default function* rootSaga() {
  yield all([
    fork(watchFetchTodo),
    fork(watchAddNewTodo)
  ]);              
}
