import config from '../config';

export const buildURL =  (endpoint) => {
  const url = config.api.url + endpoint
  return url;
}

export const buildHaders = (method = 'GET', data = null, headers = {}) => {
  const requestHeaders = (method !== 'GET' && !(data instanceof FormData)) ? Object.assign({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  }, headers) : {}
  return requestHeaders;
}