import React, { Component } from 'react';
import { connect } from 'react-redux'

import { fetchTodosAction } from '../action';

class TodoLists extends Component {
  
  componentDidMount() {
    this.props.fetchTodosAction();
  }
  render() {
    const { todos: { todos, error } } = this.props;

    if (error) return <h4>  Error...  </h4>

    if (todos.length === 0) return <h4> Loading .... </h4>
    
    return (
      <div>
        {
          todos.map((todo, idx) => {
            return (
              <li key={idx}>  
                {
                  todo.title
                }
              </li>
            )
          })
        }
      </div>
    )
  }
} 

const mapStateToProps = state => ({ todos: state.todoReducer });

export default connect(mapStateToProps, { fetchTodosAction })(TodoLists);
