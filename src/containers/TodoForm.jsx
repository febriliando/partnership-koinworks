import React, { Component } from 'react';
import { connect } from 'react-redux'

import { postTodoAction } from '../action'

class TodoForm extends Component {
  state = {
    todo: ''
  }
  handleClick = () => {
    const { todo } = this.state;
    this.props.postTodoAction(todo)
    this.setState({
      todo: ''
    })
  }
  render() {
    return (
      <div 
        style={{
          display: 'flex',
          justifyContent: 'center',
          margin: '20px auto 5px'
        }}
      >
        <input 
          onChange={ (e) =>  this.setState({ todo: e.target.value }) }
          type="text"
          placeholder="todo"
          value={this.state.todo}
        />
        <button
          onClick={this.handleClick}
        >
          Submit
        </button>
      </div>
    )
  }
}

export default connect(null, { postTodoAction })(TodoForm);