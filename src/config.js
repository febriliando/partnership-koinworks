const config = {
  api: {
    url: 'https://jsonplaceholder.typicode.com'
  }
}

export default config;