import { combineReducers } from 'redux';
import todoReducer from './todoReducer';

const allReducers = combineReducers({
  todoReducer,
  //you can add more reducers here, separated by , !
});

export default allReducers;
