const initialState = {
  todos: [],
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_SUCCESS":
      return { 
        ...state, 
        todos: action.payload
       };
    case "FETCH_FAILED":
    // console.log('disini', action.timeout)
      return {
        ...state,
        error: true
      }
    case "POST_TODO":
    console.log('di post todo')
    return state;
    case "POST_TODO_SUCCESS":
    console.log('di post success')
    return state;
    case "POST_TODO_FAILED":
    console.log('di post failed')
    return state;
    default:
      return state;
  }
};

export default reducer;