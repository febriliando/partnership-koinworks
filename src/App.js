import React, { Component } from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger'

import rootReducers from './reducers';
import TodoLists from './containers/TodoList';
import TodoForm from './containers/TodoForm';

import rootSaga from './sagas/rootSaga';
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

let store = createStore(rootReducers, composeEnhancers(applyMiddleware(sagaMiddleware, logger)));

//Redux saga

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <TodoForm />
          <TodoLists />
        </React.Fragment>
      </Provider>
    );
  }
}

sagaMiddleware.run(rootSaga);

export default App;
