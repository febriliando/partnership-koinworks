import { FETCH_TODOS, POST_TODO } from '../constants';

export const fetchTodosAction = () => {
  return {
    type: FETCH_TODOS,
    endpoint: '/todos?userId=1',
    method: 'GET',
    data: null,
    headers: {},
  }
}

export const postTodoAction = todo => {
  return {
    type: POST_TODO,
    todo
  }
}
